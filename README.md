# jUnit-vnc

Docker образ призванный облегчить и автоматизировать jUnit UI тесты использующие 
Chrome Web Browser, maven, oracle JDK 10.0.2. Основной посыл: на сервере 
развёртывается этот docker образ, далее через curl команду POST запросом в него 
загружается архив с проектом для тестирования, веб сервер принимающий файл 
распаковывает его во временную папку собирает проект и запускает тестирование. 
После окончания тестирования веб сервер очищает временные файлы и возвращает 
результаты тестирования клиенту.

## Build

Для сборки образа достаточно запустить команду:
```
docker-compose build
```

Если есть необходимость использовать реестр образов, тогда следующий вариант:

```
docker login registry.example.com
export REGISTRY_PATH=registry.example.com/team/project/
docker-compose build
docker-compose push
```

## Run

Для запуска проекта после сборки достаточно выполнить:

```
docker-compose up junit
```

Или

```
docker-compose up -d junit
```

После чего на 3000 порту будет слушать вебсервер принимающий файл проекта с тестами,
а на 6901 noVNC HTTP client, пароль vncpassword

При использовании реестра образов, команды запуска будут следующие:
```
docker login registry.example.com
export REGISTRY_PATH=registry.example.com/team/project/
docker-compose pull junit
docker-compose up junit
```

Или

```
docker login registry.example.com
export REGISTRY_PATH=registry.example.com/team/project/
docker-compose pull junit
docker-compose up -d junit
```

## Usage

Для запуска тестов, необходимо следующей командой загрузить архив с тестами:
```
curl --form "app=@./someProject.tar.gz" 'http://127.0.0.1:3000/?env=test'
```
Где `env=test` - переменная окружения используюящаяся в проекте тестирования для выбора
определённого сценария тестирования, адресов стендов и прочего. По окончанию тестов 
curl выдаст лог тестирования.

Архив с проектом тестов (`someProject.tar.gz`) должен быть примерно следующей 
структуры:
```
/src
/target
someProject.iml
pom.xml
```
Запуск которого будет осуществлун следующей командой:
```
/mvn/bin/mvn clean test -Denvironment=test
```


Если есть желание наблюдать за процессом тестирования, можно открыть noVNC HTTP client
открыв в браузере следующий url:
```
http://127.0.0.1:6901/
```
Или чтоб не запрашивало пароль:
```
http://127.0.0.1:6901/?password=vncpassword
```


## DISCLAIMER

You must accept the Oracle Binary Code License Agreement for Java SE to use this image.

Link to License: http://www.oracle.com/technetwork/java/javase/terms/license/index.html

Проект основан на слиянии следующих базовых образов в один:
* https://hub.docker.com/r/consol/ubuntu-xfce-vnc/
* https://github.com/GoodforGod/docker-debian-jdk10-oracle

И добавлении микро-веб сервера на node.js принимающего по http файла с проектом тестов.
