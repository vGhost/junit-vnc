FROM ${REGISTRY_PATH}ubuntu-xfce-vnc:base

COPY app /app

RUN cd /app \
    && yarn install \
    && yarn build \
    && rm -Rf ./node_modules ./.cache ./app.js ./webpack.config.js ./package.json ./yarn.lock \
    && chmod 0755 /app/run \
    && apt-get purge -y yarn \
    && apt -y autoremove

USER 1000

ENTRYPOINT ["/dockerstartup/vnc_startup.sh"]
CMD ["/app/run"]
