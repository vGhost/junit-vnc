const express       = require('express');
const fileUpload    = require('express-fileupload');
const fs            = require('fs');
const app           = express();
const childProcess  = require('child_process');

// default options
app.use(fileUpload());

app.post('/', function(req, res) {
    if (!req.files || !req.files.app) {
        return res.status(400).send('No files were uploaded.');
    }
    if (!req.query || !req.query.env || req.query.env.match(/[^a-z]/)) {
        return res.status(400).send('No correct env provided.');
    }

    const fPath = '/tmp/' + Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);

    try {
        fs.mkdirSync(fPath);

        // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
        const app = req.files.app;

        // Use the mv() method to place the file somewhere on your server
        app.mv(fPath + '/app.tar.gz', function (err) {
            if (err) {
                clean(fPath);
                return res.status(500).send(err);
            }

            try {
                console.log("-------- unpack data archive --------\n");
                exec('tar', ['-zxf', 'app.tar.gz'], {cwd: fPath});

                console.log("-------- execute tests --------\n");
                res.send(exec('/mvn/bin/mvn', ['clean', 'test', '-Denvironment=' + req.query.env], {cwd: fPath}));

                clean(fPath);
            } catch (err) {
                clean(fPath);
                res.send(err.toString());
            }
        });
    } catch (err) {
        clean(fPath);
        res.send(err.toString());
    }
});


function clean(fPath) {
    if (fs.existsSync(fPath)) {
        console.log("-------- clear trash --------\n");
        childProcess.exec('rm -Rf ' + fPath);
    }
}


function exec(cmd, args, options) {
    const spawn = childProcess.spawnSync(cmd, args, options);
    const text = spawn.stdout
        ? spawn.stdout.toString().trim()
        : null
    ;
    const errorText = spawn.stderr
        ? spawn.stderr.toString().trim()
        : null
    ;

    if (spawn.status !== 0 || errorText) {
        throw new Error("\n----- STD OUT: ----\n\n" + text + "\n\n----- STD ERR: ----\n\n" + errorText
            + "\n\n---------------\n"
        );
    }

    return text;
}



app.get('/', function (req, res) {
    res.send('Hello World!');
});



app.listen(3000, function () {
    console.log('jUnit helper app listening on port 3000!');
});