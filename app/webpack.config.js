const webpack = require('webpack');
const path = require('path');
const fs = require('fs');

const nodeModules = {
    /*'pg-native': 'commonjs pg-native',
    'sqlite3': 'commonjs sqlite3',*/
};


module.exports = {
    node: {
        __dirname: false
    },
    mode: 'production',
    entry: ['./app.js'],
    target: 'node',
    output: {
        filename: 'index.js',
        path: path.resolve(__dirname)
    },
    externals: nodeModules,
    optimization:{
        minimize: true,
    },
    plugins:[
        new webpack.LoaderOptionsPlugin({
            minimize: true,
            debug: false,
        }),
    ],
    module: {
        rules: [
            {
                test: [/\.js$/],
                enforce: 'pre',
            },
        ],
    },
};





